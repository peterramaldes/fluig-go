package main

import (
	"log"
	"net/http"

	"gitlab.com/peterramaldes/fluig/config"
	"gitlab.com/peterramaldes/fluig/controllers"

	"github.com/gorilla/mux"
)

func main() {
	config.InitDatabase()

	r := mux.NewRouter().StrictSlash(true)
	r.Use(commonMiddleware)

	r.HandleFunc("/vehicles", controllers.GetAllVehicle).Methods("GET")
	r.HandleFunc("/vehicles/{id}", controllers.GetVehicle).Methods("GET")
	r.HandleFunc("/vehicles", controllers.CreateVehicle).Methods("POST")
	r.HandleFunc("/vehicles/{id}", controllers.UpdateVehicle).Methods("PUT")
	r.HandleFunc("/vehicles/{id}", controllers.DeleteVehicle).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":3000", r))
}

func commonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}
