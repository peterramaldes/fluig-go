package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/peterramaldes/fluig/config"
	"gitlab.com/peterramaldes/fluig/models"
)

func CreateVehicle(w http.ResponseWriter, r *http.Request) {
	var newVehicle models.Vehicle
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "error")
	}
	json.Unmarshal(body, &newVehicle)

	config.DB.Create(&newVehicle)

	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(newVehicle)
}

func GetAllVehicle(w http.ResponseWriter, r *http.Request) {
	var vehicles []models.Vehicle

	config.DB.Find(&vehicles)

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(vehicles)
}

func GetVehicle(w http.ResponseWriter, r *http.Request) {
	p := mux.Vars(r)
	var v models.Vehicle

	config.DB.First(&v, p["id"])

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(v)
}

func UpdateVehicle(w http.ResponseWriter, r *http.Request) {
	var v models.Vehicle
	json.NewDecoder(r.Body).Decode(&v)

	config.DB.Save(&v)

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(v)
}

func DeleteVehicle(w http.ResponseWriter, r *http.Request) {
	p := mux.Vars(r)
	config.DB.Where("id = ?", p["id"]).Delete(&models.Vehicle{})
	w.WriteHeader(http.StatusNoContent)
}
