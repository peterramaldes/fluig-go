package config

import (
	"gitlab.com/peterramaldes/fluig/models"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var DB *gorm.DB

func InitDatabase() {
	db, err := gorm.Open(sqlite.Open("fluig.db"), &gorm.Config{})
	if err != nil {
		panic("Failed to connect in database")
	}

	// Migrate
	db.AutoMigrate(&models.Vehicle{})

	DB = db
}
