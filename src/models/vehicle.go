package models

type Vehicle struct {
	ID                  uint    `gorm:"primaryKey" json:"id"`
	Name                string  `json:"name"`
	Brand               string  `json:"brand"`
	Model               string  `json:"model"`
	ManufacturingDate   string  `json:"manufacturing_date"`
	AverageFuelCity     float32 `json:"avarage_fuel_city"`
	AverageFuelHighways float32 `json:"avarage_fuel_highway"`
}
