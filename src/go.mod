module gitlab.com/peterramaldes/fluig

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/mattn/go-sqlite3 v1.14.2 // indirect
	gorm.io/driver/sqlite v1.1.0
	gorm.io/gorm v1.9.19
)
