---
Title: TOTVS Fluig Test in Golang
Summary: API rest for crud vehicle and ranking spent gasoline
Language: Go
Copyright: Copyright 2020 Peter A. Ramaldes (peter.ramaldes@gmail.com)
---

# Scenario

The company “Ficticius Clean” operates in the delivery of cleaning 
products. The company comes facing major problems to forecast the fuel 
costs of their vehicles used in deliveries. To solve the problem I 
hired you to develop a Rest API. Challenge Create a vehicle 
registration API Rest to store the vehicles used by the company. The 
registration must contain the following data:

- Name
- Brand
- Model
- Manufacturing date
- Average fuel consumption within the city (KM/L)
- Average fuel consumption on highways (KM/L)

Create an API to perform the cost forecast calculation.
You should receive the following information as a parameter:
- Fuel price R$
- Total km to be traveled within the city
- Total km to be traveled on highways

The return should be a ranked list of the company's vehicles taking 
into account consideration the amount spent on fuel. The return should 
have the following data:

- Name
- Brand
- Model
- Year
- Amount of fuel spent
- Total amount spent on fuel
- Evaluation

The evaluation will take into account the following criteria:
- Code quality
- API usability experience
- Assertiveness in the calculated values
